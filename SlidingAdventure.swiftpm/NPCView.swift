import SwiftUI

struct NPCView: View {
    var body: some View {
        Image("OldMan800")
    }
}

struct NPCView_Previews: PreviewProvider {
    static var previews: some View {
        NPCView()
    }
}
