import SwiftUI

struct BackgroundView: View {
    
    var bgImage : String
    var bgXUnitPoint : Double
    var body: some View {
        Image(bgImage)
            .scaleEffect(x: 2.0, y: 1.0, anchor: UnitPoint(x: bgXUnitPoint, y: 0.5))
    }
}
