import SwiftUI

struct ContentView: View {
    
    @State var HSlide = (0.0)
    @State var facingRight = true
    @State var walkImages = ["Walk1", "Walk2", "Walk3", "Walk4"]
    @State var sideOffset = 0
    @State var bgPosition = 0
    @State var walkCount = 0
    @State var moveRight = false
    @State var moveLeft = false
    @State var oldManXUnitPoint = 1.0
    
    let OldMan = NPCView()
    
    
    
    let timer = Timer.publish(every: 0.01, on: .main, in: .common).autoconnect()
    
    var body: some View {
        
        VStack {
            ZStack{
                BackgroundView(bgImage: "NewMountains", bgXUnitPoint: HSlide)
                    .onReceive(timer) { _ in
                        if moveRight == true && HSlide<1 {
                            HSlide += 0.0003
                            walk()
                            
                        }
                        if moveLeft == true && HSlide>0 {
                            HSlide -= 0.0003
                            walk()
                            
                        } 
                    }
                
                if HSlide >= 0.25 && HSlide <= 0.75 {
                    OldMan
                        .scaleEffect(x: 0.1, y: 0.1, anchor: UnitPoint(x: (oldManXUnitPoint - ((HSlide-0.27)*10)), y: 0.75))
                }
                
                if HSlide >= 0.3 && HSlide <= 0.32 {
                    
                    Text("Hello, I have been waiting\n for you.")
                        .padding()
                        .background(.white)
                        .cornerRadius(15)
                        .offset(x: 50.0, y: 50.0)
                }
                
                
                Image(walkImages[walkCount])
                    .offset(CGSize(width: sideOffset, height: 500))
                    .scaleEffect(0.3, anchor: UnitPoint(x: facingRight ? 0.0 : 1.0, y: 0.7))
                    .rotation3DEffect(Angle(degrees: facingRight ? 0.0 : 180.0), axis: (x: 0, y: 1, z: 0))
                HStack {
                    Rectangle()
                        .fill(.white)
                    Rectangle()
                        .frame( minWidth: 800, minHeight: 300, alignment: .center)
                        .opacity(0.0)
                    //.border(Color.red, width: 20)
                    Rectangle()
                        .fill(.white)
                }
                
                
            }
            
            HStack{
                
                Image(systemName: "chevron.left.square.fill")
                    .bold()
                    .font(.system(size: 50))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .simultaneousGesture(
                        DragGesture(minimumDistance: 0)
                            .onChanged({ _ in
                                facingRight = false
                                moveLeft=true
                            })
                            .onEnded({ _ in
                                moveLeft=false
                            })
                    )
                
                Text("\(HSlide)")
                
                Image(systemName: "chevron.right.square.fill")
                    .bold()
                    .font(.system(size: 50))
                    .frame(maxWidth: .infinity, alignment: .trailing)
                
                    .simultaneousGesture(
                        DragGesture(minimumDistance: 0)
                            .onChanged({ _ in
                                facingRight = true
                                moveRight=true
                                    })
                            .onEnded({ _ in
                                moveRight=false
                                    })
                    )
            }
        }//.ignoresSafeArea()
            
        //Slider(value: $HSlide, in: 0...1)
        
        Spacer(minLength: 30)
        
            
        
    }
    func walk() {
        bgPosition = Int(HSlide*1000) % 20
        if bgPosition >= 0 && bgPosition < 5 {
            walkCount = 0   
        } else if bgPosition >= 5 && bgPosition < 10 {
            walkCount = 1 
        } else if bgPosition >= 10 && bgPosition < 15 {
            walkCount = 2
        } else if bgPosition >= 15 && bgPosition < 20 {
            walkCount = 3 
        }
        
    }
}
